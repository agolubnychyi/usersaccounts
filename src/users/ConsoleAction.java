package users;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by T on 18.11.2016.
 */
class ConsoleAction  {
    private static boolean isRunning = true;
    Scanner input;

   ConsoleAction() {
       Constants c = new Constants();
       c.getListCommands();
        input = new Scanner(System.in);
        while (isRunning) {
            String inp = input.nextLine().toLowerCase().trim();
            if (inp.equals("exit")) {
                isRunning = false;
            }else{
                if(inp.equals("add")){
                    createUser();
                }else{
                    if (inp.equals("delete")){
                        deleteUser();
                    }else{
                        if(inp.equals("list")){
                            getUsersList();
                        }else {
                            if (inp.equals("help")){
                                c.getListCommands();
                            }
                        }
                    }
                }
            }
        }
   }

    public void createUser(){

       UserBuilder newuser = new UserBuilder();

            System.out.println("Please enter Name: ...");
            newuser.setName(input.nextLine());

            System.out.println("Please enter Surname: ...");
            newuser.setSurname(input.nextLine());

            System.out.println("Please enter email: ...");
            newuser.setMail(input.nextLine());

            System.out.println("Please enter phone(only digits): ...");
            newuser.setPhone(input.nextInt());

        DB.writeFile(newuser);
        System.out.println("User successful created");
    }

    public void deleteUser(){
        System.out.println("Please enter user number...");
        int number = input.nextInt();

        ArrayList<String> list = DB.readFile();
        if (list.size() == 0){
            System.out.println("Your DB is empty or not exist");
            return;
        }else {
            if (number > list.size()) {
                System.out.println("Such user not exist. " + "Try range between 1 - " + list.size());
            } else {
                list.remove(number - 1);
                System.out.println("User successful deleted");
                DB.updateFile(list);
            }
        }
    }


    private void getUsersList(){
        ArrayList<String> list = DB.readFile();
        int usersNumber = list.size();
        int val = 0;
        while(usersNumber != 0){
            System.out.println(list.get(val));
            usersNumber--;
            val++;
        }
    }

}
