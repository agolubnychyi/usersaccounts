package users;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by T on 18.11.2016.
 */
public class DB {


    static public void writeFile(UserBuilder newuser) {

        ArrayList<String> usersList = readFile();

        String newData = "User name: " + newuser.getName()
                + " User surname: " + newuser.getSurname()
                + " User email: " + newuser.getMail()
                + " User phone: " + newuser.getPhone();

        usersList.add(newData);
        int numberOfUsers = usersList.size();

        try {
            File fileDirectory = new File("Z:\\usersAccount.txt");
            FileWriter fileWriter = new FileWriter(fileDirectory);
            BufferedWriter writer = new BufferedWriter(fileWriter);

            int i = 0;
            while (numberOfUsers > 0) {
                writer.write(usersList.get(i));
                writer.newLine();
                numberOfUsers--;
                i++;
            }
            writer.close();

        } catch (IOException ex) {
            System.out.println("Problem with writing file. Check if it exist on your local machine");
        }
    }


    static public ArrayList readFile() {

        ArrayList<String> usersList = new ArrayList<>();

        try {
            File fileDirectory = new File("Z:\\usersAccount.txt");
            FileReader fileReader = new FileReader(fileDirectory);
            BufferedReader reader = new BufferedReader(fileReader);

            String line = null;

            while ((line = reader.readLine()) != null) {
                usersList.add(line);
            }
            reader.close();

        } catch (Exception e) {
            System.out.println("Problem with reading file. Check if it exist on your local machine");
        }
        return usersList;
    }


    static public void updateFile(ArrayList list) {

        ArrayList<String> newList = list;
        int numberOfUsers = newList.size();

        try {
            File fileDirectory = new File("Z:\\usersAccount.txt");
            FileWriter fileWriter = new FileWriter(fileDirectory);
            BufferedWriter writer = new BufferedWriter(fileWriter);

            int i = 0;
            while (numberOfUsers > 0) {
                writer.write(newList.get(i));
                writer.newLine();
                numberOfUsers--;
                i++;
            }

            writer.newLine();

            writer.close();
        } catch (IOException ex) {
            System.out.println("Problem with writing file. Check if it exist on your local machine");
        }
    }
}