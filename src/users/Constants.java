package users;

import java.util.ArrayList;

/**
 * Created by T on 19.11.2016.
 */
public class Constants {
    // I don't know why this shit is present here:)

    String decorator = "==========================================";
    final ArrayList<String> cDescription = new ArrayList<>();
    final ArrayList<String> cName = new ArrayList<>();

    Constants() {

        cDescription.add("Create new user:");
        cDescription.add("Delete existed user:");
        cDescription.add("Get list of all users in DB:");
        cDescription.add("Close programme:");
        cDescription.add("List of all commands:");

        cName.add("Add");
        cName.add("Delete");
        cName.add("List");
        cName.add("Exit");
        cName.add("Help");
    }

    public void getListCommands() {


        System.out.println(decorator);
        System.out.printf("%35s%n", "List of all possible commands");
        System.out.println(decorator);

        for (int i = 0; i <cDescription.size(); i++) {
                System.out.printf("%-28s", cDescription.get(i));
                System.out.printf("%14s%n", cName.get(i));

            }

        System.out.println(decorator);
    }
}